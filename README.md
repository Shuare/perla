**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

Cuando haya terminado, puede eliminar el contenido de este README y actualizar el archivo con detalles para que otros comiencen con su repositorio.
En
## Editar un archivo

Comenzará por editar este archivo README para aprender cómo editar un archivo en Bitbucket.
En
1. Verá el botón de clonar debajo del encabezado ** Fuente ** . Haga clic en ese botón.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Abra el directorio que acaba de crear para ver los archivos de su repositorio.
En
Ahora que está más familiarizado con su repositorio de Bitbucket, continúe y agregue un nuevo archivo localmente. Puede [enviar su cambio a Bitbucket con SourceTree] (https://confluence.atlassian.com/x/iqyBMg), o puede [agregar, confirmar,] (https://confluence.atlassian.com/x/ 8QhODQ) y [presione desde la línea de comando] (https://confluence.atlassian.com/x/NQ0z